- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSArray *ws = [[UIApplication sharedApplication] windows];
    for(UIView *w in ws){
        NSArray *vs = [w subviews];
        for(UIView *v in vs){
            if([[NSString stringWithUTF8String:object_getClassName(v)] isEqualToString:@"UIPeripheralHostView"]){
                v.backgroundColor = [UIColor redColor];
            }
        }
    }
}